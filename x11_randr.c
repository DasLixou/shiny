#include "x11_randr.h"

#include "global.h"

#include <X11/extensions/Xrandr.h>

#include <stdbool.h>

void x11_randr_setup(struct global *const g)
{
	Display *const dpy = g->x11.dpy;

	bool const have_randr = !! XRRQueryExtension(
		dpy,
		&g->x11.randr.first_event,
		&g->x11.randr.first_error);

	g->x11.randr.is_supported = have_randr;

	if(!have_randr)
		return;

	XRRQueryVersion(
			dpy,
			&g->x11.randr.major_version,
			&g->x11.randr.minor_version);

	return;
}
