#include "cmdline.h"
#include "x11_display.h"
#include "x11_randr.h"
#include "x11_mainloop.h"

#include "global.h"

#include <stdlib.h>

// TODO remove
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	int rc = 1;

	struct global *const g = malloc(sizeof *g);
	if(!g)
		goto fail_global;

	g->argc = argc;
	g->argv = argv;

	list_init(&g->x11.managed_roots);

	if(!cmdline_setup(g))
		goto fail_args;

	if(!x11_display_setup(g))
		goto fail_display;

	x11_randr_setup(g);

	rc = 0;

	{
		char buffer[128];
		Window root_window = container_of(g->x11.managed_roots.next, struct x11_managed_root, siblings)->id;
		sprintf(buffer, "(sleep 2; xprop -id 0x%lx -format shiny 8b -set shiny True) &", root_window);
		system(buffer);
	}

	x11_mainloop(g);

	x11_display_teardown(g);

fail_args:
	list_destroy(&g->x11.managed_roots, struct x11_managed_root, siblings);

fail_display:
	free(g);

fail_global:
	return rc;
}
