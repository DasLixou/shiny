#pragma once

#include <stdbool.h>

struct list
{
	struct list *next, *prev;
};

static inline void list_init(struct list *const list)
{
	list->next = list->prev = list;
}

static inline bool list_empty(struct list const *const list)
{
	return list->next == list;
}

#define list_foreach(head, iter) \
	for(struct list *next_, *iter = (head)->next; next_ = iter->next, iter != (head); iter = next_)

#define container_of(ptr, type, member) \
	((type *)(((char *)ptr) - offsetof(type, member)))

static inline void list_append(
		struct list *const list,
		struct list *const elem)
{
	// list->prev points to the last element. If the list is empty, the list
	// head is the last element.
	elem->prev = list->prev;
	// the (new) last element's next pointer points to the list head.
	elem->next = list;
	// the (old) last element's next pointer points to the new element.
	list->prev->next = elem;
	// the list's "last element" pointer points to the (new) last element.
	list->prev = elem;
}

static inline struct list *list_unlink(
		struct list *const elem)
{
	struct list *const next = elem->next;

	elem->next->prev = elem->prev;
	elem->prev->next = elem->next;

	return next;
}

#define list_destroy(head, type, member) \
	do \
	{ \
		list_foreach(head, elem_) \
		{ \
			list_unlink(elem_); \
			free(container_of(elem_, type, member)); \
		} \
	} while(0)
