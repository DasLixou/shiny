#include "global.h"

#include <stdlib.h>

#include <stdio.h>

#include <string.h>

bool cmdline_setup(struct global *const g)
{
	int const argc = g->argc;
	char **const argv = g->argv;

	enum
	{
		Initial,
		ExpectRootWindow
	} state = Initial;

	for(int i = 1; i < argc; ++i)
	{
		switch(state)
		{
		case Initial:
			if(!strcmp("-root", argv[i]))
				state = ExpectRootWindow;
			else
			{
				fprintf(stderr, "Unknown argument %s\n", argv[i]);
				goto fail_args;
			}
			break;
		case ExpectRootWindow:
			{
				char *endptr;
				Window const id = strtol(argv[i], &endptr, 0);
				if(endptr[0])
				{
					fprintf(stderr, "Not a valid number: %s\n", argv[i]);
					goto fail_args;
				}

				struct x11_managed_root *const new_root = malloc(sizeof *new_root);
				if(!new_root)
					goto fail_alloc;

				new_root->id = id;

				list_append(&g->x11.managed_roots, &new_root->siblings);
				state = Initial;
			}
		}
	}

	switch(state)
	{
	case Initial:
		// okay
		break;

	case ExpectRootWindow:
		fprintf(stderr, "Option -root requires an argument\n");
		goto fail_args;
	}

	return true;

fail_alloc:
fail_args:
	// list cleanup is done by global code

	return false;
}
