#pragma once

#include <stdbool.h>

struct global;

bool x11_display_setup(struct global *);
void x11_display_teardown(struct global *);
