#pragma once

#include <stdbool.h>

struct global;

bool cmdline_setup(struct global *const g);
