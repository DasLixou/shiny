#include "x11_mainloop.h"

#include "global.h"

void x11_mainloop(struct global *const g)
{
	bool run = true;
	while(run)
	{
		Display *const dpy = g->x11.dpy;
		XEvent e;
		XNextEvent(dpy, &e);
		switch(e.type)
		{
		case PropertyNotify:
			{
				XPropertyEvent const *pe = (XPropertyEvent const *)&e;

				if(pe->atom == g->x11.self)
					run = false;
			}
			break;
		}
	}
}
