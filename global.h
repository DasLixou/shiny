#pragma once

#include "list.h"

#include <X11/Xlib.h>

#include <stdbool.h>

struct x11_managed_root
{
	struct list siblings;
	Window id;
	int x, y, w, h;
};

struct global
{
	int argc;
	char **argv;

	struct
	{
		Display *dpy;

		Atom self;

		struct list managed_roots;

		struct
		{
			bool is_supported;
			int first_event, first_error;
			int major_version, minor_version;
		} randr;
	} x11;
};
